Sphinx>=4.0.2,<5.0.0
PyYAML
rst2pdf
sphinx-csv-filter>=0.3.0
sphinx-rtd-theme>=1.0.0,<2.0.0
docutils
